<?php

use Illuminate\Database\Seeder;

class NomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paises = array(
			['nome' => 'Brasil', 'sigla' => 'BRA', 'continente' => 'America do Sul'],
			['nome' => 'Alemanha', 'sigla' => 'ALE', 'continente' => 'Europa'],
			['nome' => 'Canada', 'sigla' => 'CAN', 'continente' => 'America do Norte'],

		);
		DB::table('pais')->insert($paises);
		$this->command->info('Cadastrando países!');
    }
}
