@extends('layout')

@section('conteudo')


<h1> Lista de países</h1>

<hr />


@foreach($paises as $pais)    
    
    <div>
        Pais: {{ $pais->nome }} --- Sigla {{ $pais->sigla }} - --- ID {{ $pais->idpais }}
        <a class="btn btn-primary btn-sm" href="{{  route('pais-alteracao', [$pais->idpais]) }}" role="button">Alterar</a>
    </div>
    <hr />
    
@endforeach

@endsection
