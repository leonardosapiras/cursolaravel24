<?php

use Illuminate\Database\Seeder;

class DadosFalsosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_BR');
        for ($i=0; $i < 10; $i++) {
          echo $faker->name, "\n";
          echo $faker->email, "\n";
        }
    }
}
